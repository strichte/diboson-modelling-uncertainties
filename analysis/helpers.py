import yodarun

# runspecs is a dict of 'label : path to run directory'
def make_runs(runspecs, order=[]):
    runs = []
    if not order:
        # order doesn't matter, just take any...
        order = runspecs.keys()
    for label in order:
        rundir = runspecs[label]
        nominal = rundir + '/rivethistos_MUR1_MUF1_PDF261000.yoda'
        pdfvars = [rundir + '/rivethistos_MUR1_MUF1_PDF261{0:03d}.yoda'.format(index + 1) for index in range(100)]
        altpdfs = [rundir + '/rivethistos_MUR1_MUF1_PDF13000.yoda', rundir + '/rivethistos_MUR1_MUF1_PDF25300.yoda', ]
        scalevars = [rundir + '/rivethistos_{0}_PDF261000.yoda'.format(scales) for scales in ['MUR0.5_MUF0.5', 'MUR1_MUF0.5', 'MUR0.5_MUF1', 'MUR2_MUF1', 'MUR1_MUF2', 'MUR2_MUF2']]
        runs.append(yodarun.Run(label, nominal, pdfvarpaths=pdfvars, scalevarpaths=scalevars, altpdfpaths=altpdfs))
    return runs
