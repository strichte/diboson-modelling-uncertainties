import numpy as np
import ROOT
import readroot

class Run(object):
    '''Information about a run whose results are stored in ROOT histogram files: labels, where to find its data, etc.'''
    def __init__(self, label, filepath, nominalpath, pdfvarpaths=[], scalevarpaths=[]):
        '''NOTE: `pdfvarpaths` are only the variations of the nominal set, not alternative sets.'''
        super(Run, self).__init__()
        self.label = label
        self.histfile = ROOT.TFile(filepath, 'r')
        self.nominalpath = nominalpath
        self.pdfvarpaths = pdfvarpaths
        self.scalevarpaths = scalevarpaths
    
    def binedges(self, identifier):
        return readroot.get_bin_edges(self.nominalpath.format(identifier), self.histfile)
    
    def nominal(self, identifier):
        return readroot.get_curve(self.nominalpath.format(identifier), self.histfile)
    
    def pdf_variation_up_down(self, identifier):
        if not self.pdfvarpaths:
            print 'FOOK'
            return self.nominal(identifier), self.nominal(identifier)
        a = readroot.get_curve(self.pdfvarpaths[0].format(identifier), self.histfile)
        b = readroot.get_curve(self.pdfvarpaths[1].format(identifier), self.histfile)
        if np.sum(a) > np.sum(b): return a, b
        return b, a
    
    def scale_variation_up_down(self, identifier):
        if not self.scalevarpaths:
            return self.nominal(identifier), self.nominal(identifier)
        a = readroot.get_curve(self.scalevarpaths[0].format(identifier), self.histfile)
        b = readroot.get_curve(self.scalevarpaths[1].format(identifier), self.histfile)
        if np.sum(a) > np.sum(b): return a, b
        return b, a
