# Analysis

To produce results, run the following commands:

```bash
./bandplots.py
./officialsampleplots.py # see comment below
```

Calling ```./officialsampleplots.py``` will currently only work on Stefan Richter's laptop, because it relies on private Python modules for reading and ROOT histograms and converting them to Numpy data structures. Email me at stefan.richter@cern.ch if you want to reproduce the results and I'll upload the missing Python modules, otherwise I'll spare myself the trouble for now.

The following must be installed:

* Yoda (comes with Rivet)
* Numpy
* Matplotlib

In case of questions email stefan.richter@cern.ch
