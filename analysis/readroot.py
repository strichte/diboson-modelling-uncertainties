# Warn user about potentially missing modules if hostname doesn't contain 'rautahirvi'
import socket
if not 'rautahirvi' in socket.gethostname():
    import warnings
    warnings.warn('Module readroot requires private Python modules on Stefan Richter\'s laptop. Contact him to get them.', Warning)

import sys
sys.path.append('/Users/stefanrichter/projects/ZZ_2016/Code/source_py/modules')
sys.path.append('/Users/stefanrichter/projects/ZZ_2016/version3-5')
import zz.histograms.histograms as hist
import observables
import aux



def get_bin_edges_and_heights(histogram_path, rootfile):
    roothist = hist.get_histogram(histogram_path.encode('ascii'), rootfile)
    binedges, areas, _, _ = aux.histo2arrays(roothist)
    heights = areas / aux.get_bin_widths(roothist)
    return (binedges, heights)

def get_bin_heights(histogram_path, rootfile):
    return get_bin_edges_and_heights(histogram_path, rootfile)[1]

def get_curve(histogram_path, rootfile):
    (binedges, heights) = get_bin_edges_and_heights(histogram_path, rootfile)
    x, y = aux.bins_to_curve(binedges, heights)
    return y

def get_bin_edges(histogram_path, rootfile):
    (binedges, heights) = get_bin_edges_and_heights(histogram_path, rootfile)
    x, y = aux.bins_to_curve(binedges, heights)
    return x
