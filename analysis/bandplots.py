#!/usr/bin/env python

# Plot observables and their uncertainty bands

import helpers
import readyoda
import yodarun
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.rcParams['lines.linewidth'] = 1.0

xlabels = {
'ZeZm_m' : r'ZZ mass',
'jet_eta_2' : r'2. jet $\eta$',
'ZeZm_dR' : r'$\Delta R$(Z, Z)',
'jet_mass_1' : r'1. jet mass',
'jet_mass_2' : r'2. jet mass',
'ZZ_dphi' : r'ZZ $\Delta\phi$',
'jet_pT_2' : r'2. jet $p_{\mathrm{T}}$',
'jet_pT_1' : r'1. jet $p_{\mathrm{T}}$',
'Ze_jet1_dR' : r'$\Delta R$(Z, jet)',
'ZZ_jet1_deta' : r'$\Delta\eta$(ZZ, jet 1)',
'jet_pT_4' : r'4. jet $p_{\mathrm{T}}$',
'jet_y_1' : r'1. jet $y$',
'jet_multi_inclusive' : r'Inclusive jet multiplicity',
'jet_y_3' : r'3. jet $y$',
'jet_y_2' : r'2. jet $y$',
'ZZ_dR' : r'$\Delta R$(Z, Z)',
'Z_pT' : r'Z $p_{\mathrm{T}}$',
'ZZ_deta' : r'$\Delta\eta$(Z, Z)',
'ZZ_pT_peak' : r'ZZ $p_{\mathrm{T}}$ (peak region)',
'ZZ_pT' : r'ZZ $p_{\mathrm{T}}$',
'jet_eta_4' : r'4. jet $\eta$',
'jet_multi_exclusive' : r'Exclusive jet multiplicity',
'jet_HT' : r'Jet $H_{\mathrm{T}}$',
'jets_deta_23' : r'$\Delta\eta$(jet 2, jet 3)',
'jet_eta_1' : r'1. jet $\eta$',
'jet_mass_4' : r'4. jet mass',
'jets_dphi_23' : r'$\Delta\phi$(jet 2, jet 3)',
'jets_deta_13' : r'$\Delta\eta$(jet 1, jet 3)',
'ZZ_jet1_dR' : r'$\Delta R$(ZZ, jet 1)',
'jets_dR_23' : r'$\Delta R$(jet 2, jet 3)',
'ZZ_phi' : r'ZZ $\phi$',
'ZZ_m' : r'ZZ mass',
'jet_eta_3' : r'3. jet $\eta$',
'ZZ_dpT' : r'$\Delta p_{\mathrm{T}}$(Z, Z)',
'jet_mass_3' : r'3. jet mass',
'Z_eta' : r'Z $\eta$',
'jets_dR_13' : r'$\Delta R$(jet 1, jet 3)',
'jet_pT_3' : r'3. jet $p_{\mathrm{T}}$',
'ZeZm_deta' : r'$\Delta\eta$(Z, Z)',
'HT' : r'$H_{\mathrm{T}}$',
'jets_deta_12' : r'$\Delta\eta$(jet 1, jet 2)',
'Zl_pT' : r'Lepton $p_{\mathrm{T}}$',
'jet_y_4' : r'4. jet $y$',
'jets_dphi_12' : r'$\Delta\phi$(jet 1, jet 2)',
'jets_dR_12' : r'$\Delta R$(jet 1, jet 2)',
'ZeZm_dphi' : r'$\Delta\phi$(Z, Z)',
'ZZ_eta' : r'ZZ $\eta$',
'ZZ_costheta_planes' : r'ZZ $\cos\theta$ planes',
'Zl_eta' : r'Lepton $\eta$',
'jets_dphi_13' : r'$\Delta\phi$(jet 1, jet 3)'
}

logplot = {
'ZeZm_m' : True,
'jet_eta_2' : False,
'ZeZm_dR' : False,
'jet_mass_1' : True,
'jet_mass_2' : True,
'ZZ_dphi' : False,
'jet_pT_2' : True,
'jet_pT_1' : True,
'Ze_jet1_dR' : False,
'ZZ_jet1_deta' : False,
'jet_pT_4' : True,
'jet_y_1' : False,
'jet_multi_inclusive' : True,
'jet_y_3' : False,
'jet_y_2' : False,
'ZZ_dR' : False,
'Z_pT' : True,
'ZZ_deta' : False,
'ZZ_pT_peak' : True,
'ZZ_pT' : True,
'jet_eta_4' : False,
'jet_multi_exclusive' : True,
'jet_HT' : True,
'jets_deta_23' : False,
'jet_eta_1' : False,
'jet_mass_4' : True,
'jets_dphi_23' : False,
'jets_deta_13' : False,
'ZZ_jet1_dR' : False,
'jets_dR_23' : False,
'ZZ_phi' : False,
'ZZ_m' : True,
'jet_eta_3' : False,
'ZZ_dpT' : False,
'jet_mass_3' : True,
'Z_eta' : False,
'jets_dR_13' : False,
'jet_pT_3' : True,
'ZeZm_deta' : False,
'HT' : True,
'jets_deta_12' : False,
'Zl_pT' : True,
'jet_y_4' : False,
'jets_dphi_12' : False,
'jets_dR_12' : False,
'ZeZm_dphi' : False,
'ZZ_eta' : False,
'ZZ_costheta_planes' : False,
'Zl_eta' : False,
'jets_dphi_13' : False,
}



ylabel = 'Cross section / abscissa unit (arbitrary units)'



# The `runs` correspond to the different setups that were used and that will be compared in the plots
# `runs` is a dictionary of "label : run_directory", where the label will appear in the legend
def plot(title, identifier, histogram_path, runs, additionalruns=[]):
    fig, (ax0, ax1, ax2, ax3) = plt.subplots(4, sharex=True, gridspec_kw = {'height_ratios':[3, 1, 1, 1]}, figsize=(6.4,6.4))
    
    #print fig.get_size_inches()
    
    hatches = ['xxxxxxx', '\\\\\\\\\\\\\\', '///////', '+++']
    
    for i, run in enumerate(runs):
        x = run.binedges(histogram_path)
        if i == 0:
            ax1.plot([x[0], x[-1]], [1., 1.], 'k-', linewidth=0.5)
            ax2.plot([x[0], x[-1]], [1., 1.], 'k-', linewidth=0.5)
            ax3.plot([x[0], x[-1]], [1., 1.], 'k-', linewidth=0.5)
        nominal = run.nominal(histogram_path)
        nominal_line = ax0.plot(x, nominal, label=run.label)
        pdfup, pdfdown = run.pdf_variation_up_down(histogram_path)
        #print pdfup[10], nominal[10], pdfdown[10]
        ax0.fill_between(x, pdfup, pdfdown, color=nominal_line[0].get_color(), alpha=0.4, linewidth=0)
        scaleup, scaledown = run.scale_variation_up_down(histogram_path)
        #print scaleup[10], nominal[10], scaledown[10]
        
        ax0.fill_between(x, scaleup, scaledown, facecolor='none', hatch=hatches[i], edgecolor=nominal_line[0].get_color(), linewidth=0)

        if i < 2:
            ax1.fill_between(x, pdfup/nominal, pdfdown/nominal, color=nominal_line[0].get_color(), alpha=0.4, linewidth=0)
            ax1.fill_between(x, scaleup/nominal, scaledown/nominal, facecolor='none', hatch=hatches[i], edgecolor=nominal_line[0].get_color(), linewidth=0)
        elif i < 3:
            ax2.fill_between(x, pdfup/nominal, pdfdown/nominal, color=nominal_line[0].get_color(), alpha=0.4, linewidth=0)
            ax2.fill_between(x, scaleup/nominal, scaledown/nominal, facecolor='none', hatch=hatches[i], edgecolor=nominal_line[0].get_color(), linewidth=0)
        elif i < 4:
            ax3.fill_between(x, pdfup/nominal, pdfdown/nominal, color=nominal_line[0].get_color(), alpha=0.4, linewidth=0)
            ax3.fill_between(x, scaleup/nominal, scaledown/nominal, facecolor='none', hatch=hatches[i], edgecolor=nominal_line[0].get_color(), linewidth=0)
    
    for run in additionalruns:
        if not len(runs) > 2: break
        reference = runs[2].nominal(histogram_path)
        variation = run.nominal(histogram_path) # / 0.01946772
        variation_line = ax2.plot(x, variation/reference, '--', label=run.label)
        #print reference/variation
        variation_line = ax0.plot(x, variation, '--', label=run.label)

    #ax0.fill_between(x[4:], high_matrixy[4:], low_matrixy[4:], color=matrixline[0].get_color(), alpha=0.4, linewidth=0)
    #totalerrorartist = ax0.fill_between(x, (1. - totaly/100.)*datay/36.07456 , (1. + totaly/100.)*datay/36.07456 , label='Total uncertainty', facecolor="none", hatch='//////', edgecolor='0.75', linewidth=0.0)
    
    fig.canvas.draw()
    ax3.set_ylim(ymin=max(ax2.get_ylim()[0], 0.8), ymax=min(ax2.get_ylim()[1], 1.2))
    ax2.set_ylim(ax3.get_ylim()) # equal to those of ax 3!
    ax1.set_ylim(ax3.get_ylim()) # equal to those of ax 3!
    #ax1.set_ylim((0.9, 1.1))
    ax1.set_ylabel('Rel. uncert.')
    ax2.set_ylabel('Rel. uncert.')
    ax3.set_ylabel('Rel. uncert.')
    #ax0.set_title(observables.titles[identifier], loc='left')
    #ax0.set_title(r'ATLAS Internal   $\sqrt{s} = 13\;\mathrm{TeV}$, $36.1\;\mathrm{fb}^{-1}$', loc='left', weight='black')
    #ax0.set_title(r'', weight='black', x=0.02, y=0.9, ha='left', transform=ax0.transAxes, size='x-large')
    if logplot[identifier]:
        ax0.set_yscale('log', nonposy='clip')
        ax0.set_ylim(ymax=ax0.get_ylim()[1]*10.)
    else:
        ax0.set_ylim(ymin=0.0, ymax=ax0.get_ylim()[1]*1.3)
    #ax1.xaxis.labelpad = 9
    #ax0.yaxis.labelpad = 10
    #ax1.yaxis.labelpad = 10
    
    #ax0.yaxis.set_label_coords(-0.09, 1.0)
    #ax1.yaxis.set_label_coords(-0.09, 0.5)
    
    ax3.set_xlabel(xlabels[identifier], ha='right', x=1.) # , size='xx-large'
    ax0.set_ylabel(ylabel, ha='right', y=1.) # , size='xx-large'
    
    # ATLAS style
    ax0.set_xlim((min(x), max(x)))
    ax0.tick_params(which='both', bottom='on', top='on', left='on', right='on', direction='in')
    ax1.tick_params(which='both', bottom='on', top='on', left='on', right='on', direction='in')
    ax1.tick_params(axis='x', pad=5)
    ax2.tick_params(which='both', bottom='on', top='on', left='on', right='on', direction='in')
    ax2.tick_params(axis='x', pad=5)
    ax3.tick_params(which='both', bottom='on', top='on', left='on', right='on', direction='in')
    ax3.tick_params(axis='x', pad=5) 
    
    ax0.legend(frameon=False)
    ax0.set_title(title, loc='left', size='medium')
    
    fig.subplots_adjust(left=0.15, right=0.95, top=0.95, bottom=0.1, hspace=0.07)    
    fig.savefig(identifier + '.pdf')










def main():
    
    title = r'$pp\, \to\, e^+e^-\mu^+\mu^-$ with SHERPA 2.2.2, ATLAS fiducial phase space'
    
    # label : path to run directory
    runspecs = {
        r'0j@NLO (unweighted)' : '../results/yoda/fast-unweighted',
        r'0j@NLO' : '../results/yoda/fast-weighted',
        r'0,1j@NLO' : '../results/yoda/weighted_01jNLO',
        r'0,1j@NLO + 2,3j@LO' : '../results/yoda/weighted',
    }
    
    runs = helpers.make_runs(runspecs, order=[r'0j@NLO (unweighted)', r'0j@NLO', r'0,1j@NLO', r'0,1j@NLO + 2,3j@LO'])
    
    additionalruns = []
    # for label, nominalyoda in zip([r'0,1j@NLO + 2,3j@LO, $\mu_{\mathrm{R}} \times 2$', r'0,1j@NLO + 2,3j@LO, $\mu_{\mathrm{R}} \,/\, 2$', r'0,1j@NLO + 2,3j@LO, $\mu_{\mathrm{F}} \times 2$', r'0,1j@NLO + 2,3j@LO, $\mu_{\mathrm{F}} \,/\, 2$'], ['../results/yoda/R4.yoda', '../results/yoda/R025.yoda', '../results/yoda/F4.yoda', '../results/yoda/F025.yoda']):
    #     additionalruns.append(yodarun.Run(label, nominalyoda))
    
    histogram_paths = readyoda.get_histogram_paths('../results/yoda/fast-unweighted/rivethistos_MUR1_MUF1_PDF261000.yoda')
    histogram_names = readyoda.get_histogram_names('../results/yoda/fast-unweighted/rivethistos_MUR1_MUF1_PDF261000.yoda')
    
    for hp, hn in zip(histogram_paths, histogram_names):
        print 'Plotting {0}...'.format(hn)
        plot(title, hn, hp, runs, additionalruns=additionalruns)
        #print '\'{0}\' : r\'{1}\''.format(hn, hn.replace('_', ' ').replace('eta', '$\\eta$').replace('pT', '$p_{\\mathrm{T}}$').replace('dphi', '$\\Delta\\phi$').replace('dR', '$\\Delta R$'))
        #print '\'{0}\' : True,'.format(hn)
        #print xlabels[hn]
        #print readyoda.get_curve(hp, 'fast-unweighted/rivethistos_MUR1_MUF1_PDF261000.yoda')
        #print readyoda.get_bin_edges(hp, 'fast-unweighted/rivethistos_MUR1_MUF1_PDF261000.yoda')



if __name__ == '__main__':
    main()
