echo 'Setting up standalone Rivet environment...'
setupATLAS
lsetup "python 2.7.9-x86_64-slc6-gcc48"
#source /afs/cern.ch/sw/lcg/external/gcc/4.8/x86_64-slc6/setup.sh
#source /afs/cern.ch/sw/lcg/external/MCGenerators_lcgcmt67c/agile/1.4.1/x86_64-slc6-gcc48-opt/agileenv.sh
source /afs/cern.ch/sw/lcg/external/MCGenerators_lcgcmt67c/rivet/2.5.3/x86_64-slc6-gcc48-opt/rivetenv.sh
echo 'Done!'
