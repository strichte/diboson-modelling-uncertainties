#!/usr/bin/env python

# Print integrated-yield uncertainties

import helpers
import readyoda
import yodarun
import numpy as np



def main():
    # label : path to run directory
    runspecs = {
        r'0j@NLO (unweighted)' : '../results/yoda/fast-unweighted',
        r'0j@NLO' : '../results/yoda/fast-weighted',
        r'0,1j@NLO' : '../results/yoda/weighted_01jNLO',
        r'0,1j@NLO + 2,3j@LO' : '../results/yoda/weighted',
    }
    
    runs = helpers.make_runs(runspecs)
    
    exclusive_jet_multiplicity_histogram_path = '/MC_ZZJETS/jet_multi_exclusive'
    
    
    max_label_length = max([len(label) for label in runspecs.keys()] + [len('SAMPLE')])
    floatformat = '{0:+.1f}'
    formstring = '{0:<' + str(max_label_length) + '} {1:<10} {2:<10}'
    
    for jet_multi, label in enumerate(['Integrated yield uncertainties (jet-inclusive)', 'Yield uncertainties in 0-jet bin', 'Yield uncertainties in 1-jet bin', 'Yield uncertainties in 2-jet bin', 'Yield uncertainties in 3-jet bin', 'Yield uncertainties in 4-jet bin']):
        print '\n' + label
        print formstring.format('SAMPLE', 'SCALE (%)', 'PDF (%)')
        for run in runs:
            scaleup, scaledown = run.yield_uncertainty_scale(exclusive_jet_multiplicity_histogram_path, jets=jet_multi-1)
            pdfup, pdfdown = run.yield_uncertainty_pdf(exclusive_jet_multiplicity_histogram_path, jets=jet_multi-1)
            print formstring.format(run.label, floatformat.format(scaleup), floatformat.format(pdfup))
            print formstring.format('', floatformat.format(scaledown), floatformat.format(pdfdown))
    


if __name__ == '__main__':
    main()
