#!/usr/bin/env python

# Plot observables and their uncertainty bands

import rootrun
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

import sys
sys.path.append('/Users/stefanrichter/projects/ZZ_2016/version3-5')
import observables


mpl.rcParams['lines.linewidth'] = 1.0


ylabel = 'Cross section / abscissa unit (arbitrary units)'



# The `runs` correspond to the different setups that were used and that will be compared in the plots
# `runs` is a dictionary of "label : run_directory", where the label will appear in the legend
def plot(title, identifier, histogram_path, runs, additionalruns=[]):
    fig, (ax0, ax1, ax2) = plt.subplots(3, sharex=True, gridspec_kw = {'height_ratios':[3, 1, 1]}, figsize=(6.4,6.4))
    
    #print fig.get_size_inches()
    
    hatches = ['\\\\\\\\\\\\\\', '///////', 'xxxxxxx']
    
    for i, run in enumerate(runs):
        x = run.binedges(histogram_path)
        if i == 0:
            ax1.plot([x[0], x[-1]], [1., 1.], 'k-', linewidth=0.5)
            ax2.plot([x[0], x[-1]], [1., 1.], 'k-', linewidth=0.5)
        nominal = run.nominal(histogram_path)
        nominal_line = ax0.plot(x, nominal, label=run.label)
        pdfup, pdfdown = run.pdf_variation_up_down(histogram_path)
        #print pdfup[10], nominal[10], pdfdown[10]
        ax0.fill_between(x, pdfup, pdfdown, color=nominal_line[0].get_color(), alpha=0.4, linewidth=0)
        scaleup, scaledown = run.scale_variation_up_down(histogram_path)
        #print scaleup[10], nominal[10], scaledown[10]
        
        ax0.fill_between(x, scaleup, scaledown, facecolor='none', hatch=hatches[i], edgecolor=nominal_line[0].get_color(), linewidth=0)

        print 'Sample:', run.label
        print 'Larger-side PDF uncertainty in %:', np.maximum((pdfup - nominal)/nominal*100., (nominal - pdfdown)/nominal*100.)[::2]
        print 'Larger-side SCALE uncertainty in %:', np.maximum((scaleup - nominal)/nominal*100., (nominal - scaledown)/nominal*100.)[::2]

        if i == 0:
            ax1.fill_between(x, pdfup/nominal, pdfdown/nominal, color=nominal_line[0].get_color(), alpha=0.4, linewidth=0)
            ax1.fill_between(x, scaleup/nominal, scaledown/nominal, facecolor='none', hatch=hatches[i], edgecolor=nominal_line[0].get_color(), linewidth=0)
        else:
            ax2.fill_between(x, pdfup/nominal, pdfdown/nominal, color=nominal_line[0].get_color(), alpha=0.4, linewidth=0)
            ax2.fill_between(x, scaleup/nominal, scaledown/nominal, facecolor='none', hatch=hatches[i], edgecolor=nominal_line[0].get_color(), linewidth=0)
        
        if identifier == 'jets_N_binning0':
            print run.label
            print 'pdf up:  ', (pdfup - nominal)/nominal
            print 'pdf down:', (pdfdown - nominal)/nominal
            print 'scale up  :', (scaleup - nominal)/nominal
            print 'scale down:', (scaledown - nominal)/nominal
        
    fig.canvas.draw()
    ax2.set_ylim(ymin=max(ax2.get_ylim()[0], 0.8), ymax=min(ax2.get_ylim()[1], 1.2))
    ax1.set_ylim(ymin=ax2.get_ylim()[0], ymax=ax2.get_ylim()[1])
    #ax1.set_ylim((0.9, 1.1))
    ax1.set_ylabel('Rel. uncert.')
    ax2.set_ylabel('Rel. uncert.')
    #ax0.set_title(observables.titles[identifier], loc='left')
    #ax0.set_title(r'ATLAS Internal   $\sqrt{s} = 13\;\mathrm{TeV}$, $36.1\;\mathrm{fb}^{-1}$', loc='left', weight='black')
    #ax0.set_title(r'', weight='black', x=0.02, y=0.9, ha='left', transform=ax0.transAxes, size='x-large')
    if observables.logplot[identifier]:
        ax0.set_yscale('log', nonposy='clip')
        ax0.set_ylim(ymax=ax0.get_ylim()[1]*10.)
    else:
        ax0.set_ylim(ymin=0.0, ymax=ax0.get_ylim()[1]*1.3)
    #ax1.xaxis.labelpad = 9
    #ax0.yaxis.labelpad = 10
    #ax1.yaxis.labelpad = 10
    
    #ax0.yaxis.set_label_coords(-0.09, 1.0)
    #ax1.yaxis.set_label_coords(-0.09, 0.5)
    
    ax2.set_xlabel(observables.xlabels[identifier], ha='right', x=1.) # , size='xx-large'
    ax0.set_ylabel(ylabel, ha='right', y=1.) # , size='xx-large'
    
    # ATLAS style
    ax0.set_xlim((min(x), max(x)))
    ax0.tick_params(which='both', bottom='on', top='on', left='on', right='on', direction='in')
    ax1.tick_params(which='both', bottom='on', top='on', left='on', right='on', direction='in')
    ax1.tick_params(axis='x', pad=5)
    ax2.tick_params(which='both', bottom='on', top='on', left='on', right='on', direction='in')
    ax2.tick_params(axis='x', pad=5)
    
    ax0.legend(frameon=False)
    ax0.set_title(title, loc='left', size='medium')
    
    fig.subplots_adjust(left=0.15, right=0.95, top=0.95, bottom=0.1, hspace=0.07)    
    fig.savefig(identifier + '.pdf')










def main():
    
    title = r'$pp\, \to\, e^+e^-\mu^+\mu^-$ official samples, ATLAS fiducial phase space'
    
    runs = []
    for label, rootfilename in zip([r'Powheg 0j@NLO', r'Sherpa (0,1j@NLO + 2,3j@LO)'], ['../results/root/processed_powhegqqbar_Signal_systematics.root', '../results/root/processed_sherpaqqbar_Signal_systematics.root']):
        nominal = 'NoChannelSelection/nominal/Unfolding/{0}/fiducial'
        pdfvars = ['NoChannelSelection/' + var + '/Unfolding/{0}/fiducial' for var in ['PDF__1up','PDF__1down']]
        scalevars = ['NoChannelSelection/' + var + '/Unfolding/{0}/fiducial' for var in ['QCD_SCALE__1up','QCD_SCALE__1down']]
        runs.append(rootrun.Run(label, rootfilename, nominal, pdfvarpaths=pdfvars, scalevarpaths=scalevars))
    
    for identifier in observables.identifiers:
        print 'Plotting {0}...'.format(identifier)
        plot(title, identifier, identifier, runs)



if __name__ == '__main__':
    main()
