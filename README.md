# Diboson modelling uncertainties

A little study of diboson modelling uncertainties. The considered process is pp → l<sup>+</sup> l<sup>-</sup> l'<sup>+</sup> l'<sup>-</sup> @ NLO, in the case of Sherpa with various multiplicities additional partons at the matrix-element level.

The research questions (with answers where already known):

1. Do **Sherpa and Powheg** predict similar scale and PDF uncertainties?
	- Multijet-merged Sherpa setups seem to predict much larger scale uncertainties than Powheg
2. Are the **Sherpa** uncertainties compatible between **weighted and unweighted** event generation?
	- For **non-multijet-merged** Sherpa setups: **yes**, I can confirm that this is the case
	- **Not yet checked for multijet-merged** Sherpa setups
3. How do the sizes of the **uncertainties for different** matrix-element-parton/**jet multiplicities** compare?
4. How does the **multijet-merging** in Sherpa **affect the uncertainties**?
	- It **seems to increase them hugely**! Perhaps normalisation in my Rivet runs was wrong? The script sets `rivet.CrossSection = 1.0`, which might not be correct!
5. Are the uncertainties from Sherpa **on-the-fly variations** **compatible with** those from **"manual" re-runs** with different parameters?

See the `results` directory for details.
