# SHERPA prediction studies for $4\ell$ ("ZZ") production

I run a private SHERPA generation of *unweighted* events to answer the question:

* Are the large QCD scale uncertainties observed in the ATLAS SHERPA $q\bar{q} \to \ell^+ \ell^- \ell'^+ \ell'^-$ sample due to technical problems with event weights, or are they correct?

By 'large' I mean much larger than the ones observed using POWHEG.

---

(I work on LXPlus in `/afs/cern.ch/user/s/strichte/workdir/zz-sherpa`)

Below I follow the ATLAS instructions for [SHERPA](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/SherpaForAtlas#On_the_fly_interface) and [RIVET](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/RivetForAtlas).

I use SHERPA to generate $pp \to e^+ e^- \mu^+ \mu^-$ events in a phase space that is not too dissimilar from the ones used in the 2016 'on-shell' ZZ measurement and the 2017 joint Higgs/SM $m_{4\ell}$ measurement, achieved by simple phase space cuts. The job option used to generate the events via ATHENA is:

```py
include("MC15JobOptions/Sherpa_2.2.2_NNPDF30NNLO_Common.py")

evgenConfig.description = "e+e-mu+mu- with 0,1j@NLO + 2,3j@LO and 66 GeV < ml+l- < 116 GeV (l+l- = e+e-, mu+mu-), pTl1>20 GeV, pTl2>15 GeV, pTl3>10 GeV, pTl4>5 GeV, abseta(l)<2.7."
evgenConfig.keywords = ["SM", "diboson", "4lepton", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "stefan.richter@cern.ch" ]
evgenConfig.minevents = 100
evgenConfig.inputconfcheck = "Sherpa_222_NNPDF30NNLO_llll"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=3; LJET:=4,5; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  EXCLUSIVE_CLUSTER_MODE=1

  AMEGIC_CUT_MASSIVE_VECTOR_PROPAGATORS=0
  SOFT_SPIN_CORRELATIONS=1
}(run)

(processes){
  Process 93 93 -> 11 -11 13 -13 93{NJET};
  Order (*,4); CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  Enhance_Factor  3 {5};
  Enhance_Factor 10 {6};
  Enhance_Factor 10 {7};
  End process;
}(processes)

(selector){
  "PT" 90 20.0,E_CMS:15.0,E_CMS:10.0,E_CMS:5.0,E_CMS [PT_UP]
  "PseudoRapidity" 90 0.0,2.7
  Mass 11 -11 66.0 116.0
  Mass 13 -13 66.0 116.0
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]
genSeq.Sherpa_i.Parameters += [ "EVENT_GENERATION_MODE=Weighted"]
```
With the final line being replaced by

```py
genSeq.Sherpa_i.Parameters += [ "EVENT_GENERATION_MODE=Unweighted"]
```
for the unweighted generation setup.

Note that QCD scale variations as well as PDF variations are performed, as this is the current ATLAS default.

The events are analysed on the fly with the existing RIVET analyses MC\_ZZINC and MC\_ZZJETS. Each weight variation is analysed separately, because RIVET doesn't yet accept multiple weights in the same analysis. The RIVET run is configured in an additional script for ATHENA:

```py
# To run this with Generate_tf.py, add the option `--postInclude rivet_config.py`

weight_variations = ['Weight', 'MUR0.5_MUF0.5_PDF261000', 'MUR0.5_MUF1_PDF261000', 'MUR1_MUF0.5_PDF261000', 'MUR1_MUF1_PDF261000', 'MUR1_MUF2_PDF261000', 'MUR2_MUF1_PDF261000', 'MUR2_MUF2_PDF261000', 'MUR1_MUF1_PDF261001', 'MUR1_MUF1_PDF261002', 'MUR1_MUF1_PDF261003', 'MUR1_MUF1_PDF261004', 'MUR1_MUF1_PDF261005', 'MUR1_MUF1_PDF261006', 'MUR1_MUF1_PDF261007', 'MUR1_MUF1_PDF261008', 'MUR1_MUF1_PDF261009', 'MUR1_MUF1_PDF261010', 'MUR1_MUF1_PDF261011', 'MUR1_MUF1_PDF261012', 'MUR1_MUF1_PDF261013', 'MUR1_MUF1_PDF261014', 'MUR1_MUF1_PDF261015', 'MUR1_MUF1_PDF261016', 'MUR1_MUF1_PDF261017', 'MUR1_MUF1_PDF261018', 'MUR1_MUF1_PDF261019', 'MUR1_MUF1_PDF261020', 'MUR1_MUF1_PDF261021', 'MUR1_MUF1_PDF261022', 'MUR1_MUF1_PDF261023', 'MUR1_MUF1_PDF261024', 'MUR1_MUF1_PDF261025', 'MUR1_MUF1_PDF261026', 'MUR1_MUF1_PDF261027', 'MUR1_MUF1_PDF261028', 'MUR1_MUF1_PDF261029', 'MUR1_MUF1_PDF261030', 'MUR1_MUF1_PDF261031', 'MUR1_MUF1_PDF261032', 'MUR1_MUF1_PDF261033', 'MUR1_MUF1_PDF261034', 'MUR1_MUF1_PDF261035', 'MUR1_MUF1_PDF261036', 'MUR1_MUF1_PDF261037', 'MUR1_MUF1_PDF261038', 'MUR1_MUF1_PDF261039', 'MUR1_MUF1_PDF261040', 'MUR1_MUF1_PDF261041', 'MUR1_MUF1_PDF261042', 'MUR1_MUF1_PDF261043', 'MUR1_MUF1_PDF261044', 'MUR1_MUF1_PDF261045', 'MUR1_MUF1_PDF261046', 'MUR1_MUF1_PDF261047', 'MUR1_MUF1_PDF261048', 'MUR1_MUF1_PDF261049', 'MUR1_MUF1_PDF261050', 'MUR1_MUF1_PDF261051', 'MUR1_MUF1_PDF261052', 'MUR1_MUF1_PDF261053', 'MUR1_MUF1_PDF261054', 'MUR1_MUF1_PDF261055', 'MUR1_MUF1_PDF261056', 'MUR1_MUF1_PDF261057', 'MUR1_MUF1_PDF261058', 'MUR1_MUF1_PDF261059', 'MUR1_MUF1_PDF261060', 'MUR1_MUF1_PDF261061', 'MUR1_MUF1_PDF261062', 'MUR1_MUF1_PDF261063', 'MUR1_MUF1_PDF261064', 'MUR1_MUF1_PDF261065', 'MUR1_MUF1_PDF261066', 'MUR1_MUF1_PDF261067', 'MUR1_MUF1_PDF261068', 'MUR1_MUF1_PDF261069', 'MUR1_MUF1_PDF261070', 'MUR1_MUF1_PDF261071', 'MUR1_MUF1_PDF261072', 'MUR1_MUF1_PDF261073', 'MUR1_MUF1_PDF261074', 'MUR1_MUF1_PDF261075', 'MUR1_MUF1_PDF261076', 'MUR1_MUF1_PDF261077', 'MUR1_MUF1_PDF261078', 'MUR1_MUF1_PDF261079', 'MUR1_MUF1_PDF261080', 'MUR1_MUF1_PDF261081', 'MUR1_MUF1_PDF261082', 'MUR1_MUF1_PDF261083', 'MUR1_MUF1_PDF261084', 'MUR1_MUF1_PDF261085', 'MUR1_MUF1_PDF261086', 'MUR1_MUF1_PDF261087', 'MUR1_MUF1_PDF261088', 'MUR1_MUF1_PDF261089', 'MUR1_MUF1_PDF261090', 'MUR1_MUF1_PDF261091', 'MUR1_MUF1_PDF261092', 'MUR1_MUF1_PDF261093', 'MUR1_MUF1_PDF261094', 'MUR1_MUF1_PDF261095', 'MUR1_MUF1_PDF261096', 'MUR1_MUF1_PDF261097', 'MUR1_MUF1_PDF261098', 'MUR1_MUF1_PDF261099', 'MUR1_MUF1_PDF261100', 'MUR1_MUF1_PDF269000', 'MUR1_MUF1_PDF270000', 'MUR1_MUF1_PDF25300', 'MUR1_MUF1_PDF13000']

import os

from Rivet_i.Rivet_iConf import Rivet_i

for weight in weight_variations:
    rivet = Rivet_i(weight.replace('.', '')) # Remove dots from string, because Gaudi does not allow them
    rivet.AnalysisPath = os.environ['PWD']
    rivet.Analyses += [ 'MC_ZZJETS' , 'MC_ZZINC' ]
    rivet.WeightName = weight # name of weight variation
    rivet.HistoFile = 'rivethistos_' + weight # unique output name for YODA histograms
    rivet.DoRootHistos = True
    rivet.Stream = 'roothistos_' + weight # unique output name for ROOT histograms
    rivet.RunName = '' # 'run_' + weight
    #rivet.CrossSection = 1.0
    genSeq += rivet
```

I use the following ATHENA setup:

```bash
asetup 19.2.5.17.1,MCProd
```

And the following commands to start the event generation locally (I do this in a `tmux` session):

```bash
Generate_tf.py --ecmEnergy=13000 --runNumber=999999 --firstEvent=1 --randomSeed=1105 --jobConfig=MC15.999999.Sherpa_222_NNPDF30NNLO_llll_weighted.py --outputEVNTFile=/tmp/strichte/weighted.pool.root --maxEvents=100000 --rivetAnas=MC_ZZINC,MC_ZZJETS |& tee weighted_stdout_stderr.log
```
```bash
Generate_tf.py --ecmEnergy=13000 --runNumber=999999 --firstEvent=1 --randomSeed=1105 --jobConfig=MC15.999999.Sherpa_222_NNPDF30NNLO_llll_unweighted.py --outputEVNTFile=/tmp/strichte/unweighted.pool.root --maxEvents=100000 --rivetAnas=MC_ZZINC,MC_ZZJETS |& tee unweighted_stdout_stderr.log
```

With RIVET analysis for each weight:

```bash
Generate_tf.py --ecmEnergy=13000 --runNumber=999999 --firstEvent=1 --randomSeed=1105 --jobConfig=MC15.999999.Sherpa_222_NNPDF30NNLO_llll_weighted.py --outputEVNTFile=/tmp/strichte/rivetall_weighted.pool.root --maxEvents=100000 --postInclude rivet_config.py |& tee weighted_stdout_stderr.log
```
```bash
Generate_tf.py --ecmEnergy=13000 --runNumber=999999 --firstEvent=1 --randomSeed=1105 --jobConfig=MC15.999999.Sherpa_222_NNPDF30NNLO_llll_unweighted.py --outputEVNTFile=/tmp/strichte/rivetall_unweighted.pool.root --maxEvents=100000 --postInclude rivet_config.py |& tee unweighted_stdout_stderr.log
```


**Important:** I use OpenLoops for loop corrections. SHERPA doesn't find/install the necessary OpenLoops libs autonomously, so one needs to install them manually somehow. I downloaded the libraries used by an official ATLAS run and copied them into my generation directory:

```bash
setupATLAS
voms-proxy-init -voms atlas # and type Grid password
lsetup rucio
rucio download group.phys-gener.sherpa020201.363490.Sherpa_221_NNPDF30NNLO_llll_13TeV.TXT.mc15_v1
tar xvf group.phys-gener.sherpa020201.363490.Sherpa_221_NNPDF30NNLO_llll_13TeV.TXT.mc15_v1/group.phys-gener.sherpa020201.363490.Sherpa_221_NNPDF30NNLO_llll_13TeV.TXT.mc15_v1._00001.tar.gz
# then take Process/OpenLoops (not the other stuff!) and copy it to Process/OpenLoops in the directory in which I run Generate_tf.py [...]
```

**Important:** sometimes the first `Generate_tf.py` call fails due to missing AMEGIC libraries. I then go in the process directory and execute `./makelibs` to compile the libraries. Then I run the same `Generate_tf.py` command again.


