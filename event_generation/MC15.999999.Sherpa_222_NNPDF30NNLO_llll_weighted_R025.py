include("MC15JobOptions/Sherpa_2.2.2_NNPDF30NNLO_Common.py")

evgenConfig.description = "e+e-mu+mu- with 0,1j@NLO + 2,3j@LO and 66 GeV < ml+l- < 116 GeV (l+l- = e+e-, mu+mu-), pTl1>20 GeV, pTl2>15 GeV, pTl3>10 GeV, pTl4>5 GeV, abseta(l)<2.7."
evgenConfig.keywords = ["SM", "diboson", "4lepton", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "stefan.richter@cern.ch" ]
evgenConfig.minevents = 100
evgenConfig.inputconfcheck = "Sherpa_222_NNPDF30NNLO_llll"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=0.25; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=3; LJET:=4,5; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  EXCLUSIVE_CLUSTER_MODE=1

  AMEGIC_CUT_MASSIVE_VECTOR_PROPAGATORS=0
  SOFT_SPIN_CORRELATIONS=1
}(run)

(processes){
  Process 93 93 -> 11 -11 13 -13 93{NJET};
  Order (*,4); CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  Enhance_Factor  3 {5};
  Enhance_Factor 10 {6};
  Enhance_Factor 10 {7};
  End process;
}(processes)

(selector){
  "PT" 90 20.0,E_CMS:15.0,E_CMS:10.0,E_CMS:5.0,E_CMS [PT_UP]
  PseudoRapidityNLO 90 -2.7 2.7
  Mass 11 -11 66.0 116.0
  Mass 13 -13 66.0 116.0
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]
genSeq.Sherpa_i.Parameters += [ "EVENT_GENERATION_MODE=Weighted"]
genSeq.Sherpa_i.Parameters += [ "SCALE_VARIATIONS=None", "PDF_VARIATIONS=None" ] # switch off on-the-fly variations
