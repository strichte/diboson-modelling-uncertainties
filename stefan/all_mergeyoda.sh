./mergeyoda.sh ../event_generation/fast-weighted ; mv *.yoda fast-weighted
./mergeyoda.sh ../event_generation/fast-unweighted ; mv *.yoda fast-unweighted
./mergeyoda.sh ../event_generation/weighted ; mv *.yoda weighted
./mergeyoda-nosyst.sh ../event_generation/manualvariations/R4
./mergeyoda-nosyst.sh ../event_generation/manualvariations/R025
./mergeyoda-nosyst.sh ../event_generation/manualvariations/F4
./mergeyoda-nosyst.sh ../event_generation/manualvariations/F025
