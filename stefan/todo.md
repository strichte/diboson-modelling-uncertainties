# Uncertainties in SHERPA

## Stefan's tasks and their status

- Try a sample with 0,1j@NLO, i.e. without the 2,3j components
(NJET:=1; LJET:=4,5)
   - Started compiling & integrating 17 July (late evening)

- Let's focus on the exclusive jet multi plot, because there it
becomes most obvious. Is it possible to include in this plot the
sub-contributions from different ME multiplicities in the MEPS@NLO
samples? To do that, you can split the events depending on their
number of status==3 particles, those match almost always the ME
multiplicity + 1 (in rare cases of MC@NLO S-events without shower
emission they would be without the +1, but we can ignore that).
With that we should be able to see which ME jet multi contributes most
to the high uncertainty in the low exclusive jet multi bins.
   - Cannot find way to access status code in Rivet :(
   - I can do this with the official ATLAS samples, but requires rerunning my analysis code with theory systematics enabled (takes a couple of days for the large Sherpa sample)

- Not as urgent, but if you have CPU/time capacity: you could do a
merged run with explicit scale variations (instead of OTF) to repeat
the kind of closure test we have done in V+jets. For example to
explicitly vary the renormalisation scale by 0.5 and 2 you can change
to RSF:=0.25 and RSF:=4.0 in the line at the beginning of your run
card. The muR variations will probably be dominant, so it could
suffice to closure-test them alone.
    - Started generating on 17 July (late evening), reusing integration grids from runs with variations


- As for Josh's question whether there can be any funny business going
on with the overall normalisations: Stefan, how are you treating the
normalisation in the plots you show?
    - Responded 17 July early evening
