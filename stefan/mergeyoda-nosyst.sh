#!/usr/bin/env bash
# Give generation directory as command line argument!

yodamerge -o $(basename $1).yoda ${1}/run_*/Rivet.yoda
