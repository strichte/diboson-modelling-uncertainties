#!/usr/bin/env bash
scp *.py setuprivet.sh strichte@lxplus.cern.ch:/afs/cern.ch/user/s/strichte/workdir/zz-sherpa/event_generation
scp *.py strichte@uclgateway:/home/strichte/projects/Sherpa/event_generation
scp setuprivet.sh mergeyoda.sh strichte@uclgateway:/home/strichte/projects/Sherpa/analysis
